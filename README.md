# Reproduction Package of the paper "Making CodeCity Evolve" for Benevol'19

## See directly the example

1. Go to https://thesis-dlumbrer.gitlab.io/repr-pckg-benevol-2019 (static page of this repo)
2. Click on the item `Time Evolution of the Angular project` 
3. Wait to load the City and the time evoltion

## Create the city

### First step: ElasticSearch with data stored from a Graal cocom analysis

1. Execute Graal (`https://github.com/chaoss/grimoirelab-graal`) with the CoCom analysis.

    - You can follow the [How to use](https://github.com/chaoss/grimoirelab-graal#how-to-use) guide, specifically, the CoCom backend.

2. Then, check that there is the data in the ElasticSearch where Graal stored the data.

### Second step: Genereate city data

0. **Optional**: Make a python 3 [Virtual Env](https://docs.python.org/3/library/venv.html) for this test
1. Go to `generate_city` folder.
2. Install requeriments: `pip3 install -r requeriments.txt`
3. Execute the file `generate_structure_codecityjs.py` following one of these ways:
    - If you have an ElasticSearch with the data analyzed with Graal Cocom: 
        ```
        python3 generate_structure_codecityjs.py
        -time <days_snapshop_for_time_evolution>
        --repo <repo_name>
        --export-enriched-dataframe
        --export-dataframe
        -e <elasticsearch_url>
        -i <elasticsearch_index>
        ```
    - If you want to use the elasticsearch index backup provided in this repo with data from the [Angular](https://github.com/angular/angular) project: 
        ```
        python3 generate_structure_codecityjs.py
        -time <days_snapshop_for_time_evolution>
        --repo https://github.com/angular/angular
        --export-enriched-dataframe
        --export-dataframe
        -if data/index_backup_graal_cocom_incubator_angular.json
        ```
    - If you want to use the dataframe backup provided in this repo with data from the [Angular](https://github.com/angular/angular) project: 
        ```
        python3 generate_structure_codecityjs.py
        -time <days_snapshop_for_time_evolution>
        --repo https://github.com/angular/angular
        --export-enriched-dataframe
        -df data/index_dataframe_graal_cocom_incubator_angular.csv
        ```
    - If you want to use the dataframe backup provided in this repo with data from the [Angular](https://github.com/angular/angular) project (**QUICKEST WAY**): 
        ```
        python3 generate_structure_codecityjs.py
        -time <days_snapshop_for_time_evolution>
        --repo https://github.com/angular/angular
        -edf data/index_dataframe_graal_cocom_incubator_enriched_angular.csv
        ```
    


### Third step: see the result

0. **Optional**: Launch an HTTP server inside the public page (p.e python Simple HTTP Server)
1. Go to the index.html server of the public folder (`./public/index.html`) in a browser (with a http server launched)